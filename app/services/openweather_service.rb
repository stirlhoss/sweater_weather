class OpenweatherService
  def self.get_weather(lat, lng)
    response = Faraday.get('https://api.openweathermap.org/data/2.5/onecall') do |faraday|
      faraday.params['APPID'] = ENV['openweather_api_key']
      faraday.params['lat'] = lat
      faraday.params['lon'] = lng
      faraday.params['units'] = 'imperial'
    end
    JSON.parse(response.body, symbolize_names: true)
  end
end
