class YelpService
  class << self
    def get_bussinesses(location, food)
      call_api("/v3/businesses/search?location=#{location}&categories=#{food}")
    end

  private

    def call_api(path)
      response = conn.get(path)
      parsed_data(response)
    end

    def conn
      Faraday.new(url: 'https://api.yelp.com') do |faraday|
        faraday.headers['Authorization'] = ENV['yelp_api_key']
      end
    end

    def parsed_data(response)
      JSON.parse(response.body, symbolize_names: true)
    end
  end
end
