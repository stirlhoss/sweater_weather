class MapquestService
  class << self
    def get_geocode(address)
      call_api("/geocoding/v1/address?location=#{address}")
    end

    def get_reverse_geocode(geocode)
      call_api("/geocoding/v1/reverse?location=#{geocode}")
    end

    def get_route(from, to)
      call_api("/directions/v2/route?to=#{to}&from=#{from}")
    end

  private

    def call_api(path)
      response = conn.get(path)
      parsed_data(response)
    end

    def conn
      conn = Faraday.new(url: 'http://www.mapquestapi.com') do |faraday|
        faraday.params['key'] = ENV['mapquest_api_key']
      end
    end

    def parsed_data(response)
      JSON.parse(response.body, symbolize_names: true)
    end

  end
end
