class ImpossibleTripSerializer
  def self.impossible_trip(origin, destination)
    {
      data: {
        id: nil,
        type: 'road_trip',
        attributes: {
          start_city: origin.to_s,
          end_city: destination.to_s,
          travel_time: 'Impossible',
          weather_at_eta: {
            temperature: '',
            conditions: ''
          }
        }
      }
    }
  end
end
