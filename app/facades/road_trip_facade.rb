class RoadTripFacade
  class << self
    def get_road_trip(origin, destination)
      trip_data = MapquestService.get_route(origin, destination)
      if trip_data[:info][:statuscode] == 402
        return {error: 'Impossible trip'}
      else
      geocode = MapquestFacade.get_geocode(destination)
      weather_data = OpenweatherService.get_weather(geocode.lat, geocode.lng)
      RoadTrip.new(trip_data, weather_data)
      end
    end
  end
end
