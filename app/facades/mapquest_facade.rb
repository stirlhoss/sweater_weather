class MapquestFacade
  class << self
    def get_geocode(location)
      data = MapquestService.get_geocode(location)
      Geocode.new(data)
    end
  end
end
