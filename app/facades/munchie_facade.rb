class MunchieFacade
  class << self
    def get_munchie(location, food)
      geocode = MapquestFacade.get_geocode(location)
      weather_data = OpenweatherService.get_weather(geocode.lat, geocode.lng)
      yelp = YelpService.get_bussinesses(location, food)
      Munchie.new(location, yelp, weather_data)
    end
  end
end
