class WeatherFacade
  class << self
    def get_weather(location)
      geocode = MapquestFacade.get_geocode(location)
      weather_data = OpenweatherService.get_weather(geocode.lat, geocode.lng)
      Weather.new(weather_data)
    end
  end
end
