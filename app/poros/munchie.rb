class Munchie
  attr_reader :id,
              :forecast,
              :restaurant,
              :destination_city

  def initialize(location, yelp, weather)
    @id = nil
    @forecast = yelp_weather(weather)
    @restaurant = yelp_info(yelp)
    @destination_city = location.titleize
  end

  def yelp_weather(weather)
    current = weather[:current]
    {
      summary: current[:weather][0][:description],
      temperature: current[:temp]
    }
  end

  def yelp_info(yelp)
    info = yelp[:businesses][0]
    {
      name: info[:name],
      address: info[:location][:display_address][0] + ', ' + info[:location][:display_address][1]
    }
  end
end
