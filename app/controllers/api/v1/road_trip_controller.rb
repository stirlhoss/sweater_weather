class Api::V1::RoadTripController < ApplicationController
  def create
    user = User.find_by(api_key: params[:api_key])
    if user == nil
      render json: { error: 'Invalid key' }, status: 401
    else
      trip = RoadTripFacade.get_road_trip(params[:origin], params[:destination])
      if trip.class == Hash
        render json: ImpossibleTripSerializer.impossible_trip(params[:origin], params[:destination]), status: 400
      else
        render json: RoadTripSerializer.new(trip)
      end
    end
  end
end
