class Api::V1::MunchiesController < ApplicationController
  def index
    if params[:location] && params[:food]
      munchie = MunchieFacade.get_munchie(params[:location], params[:food])
      render json: MunchieSerializer.new(munchie)
    else
      render json: { errors: { details: 'Invalid params' } }, status: 400
    end
  end
end
