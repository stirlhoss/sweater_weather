class User < ApplicationRecord
  validates :email, uniqueness: true, presence: true
  validates :password_digest, presence: true
  has_many :api_keys, as: :bearer, dependent: :destroy
  has_secure_token :api_key

  has_secure_password
end
