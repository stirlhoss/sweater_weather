require 'rails_helper'

RSpec.describe MapquestService do
  it 'returns geocode response when given address', :vcr do
    response = MapquestService.get_geocode('peoria, il')

    expect(response).to be_a Hash
    expect(response[:results]).to be_an Array
    expect(response[:results][0]).to have_key :locations
    expect(response[:results][0][:locations][0][:adminArea5]).to eq('Peoria')
    expect(response[:results][0][:locations][0][:adminArea3]).to eq('IL')
    expect(response[:results][0][:locations][0]).to have_key :latLng
    expect(response[:results][0][:locations][0][:latLng][:lat]).to eq(40.694592)
    expect(response[:results][0][:locations][0][:latLng][:lng]).to eq(-89.590362)
  end

  it 'returns address response when given geocode', :vcr do
    response = MapquestService.get_reverse_geocode('40.694592, -89.590362')

    expect(response).to be_a Hash
    expect(response[:results]).to be_an Array
    expect(response[:results][0]).to have_key :locations
    expect(response[:results][0][:locations][0][:adminArea5]).to eq('Peoria')
    expect(response[:results][0][:locations][0][:adminArea3]).to eq('IL')
    expect(response[:results][0][:locations][0]).to have_key :latLng
    expect(response[:results][0][:locations][0][:latLng][:lat]).to eq(40.694592)
    expect(response[:results][0][:locations][0][:latLng][:lng]).to eq(-89.590362)
  end

  it 'returns directions response when given two locations', :vcr do
    response = MapquestService.get_route('peoria, il', 'denver, co')

    expect(response).to be_a Hash
    expect(response[:route][:distance]).to eq 935.151
    expect(response[:route][:formattedTime]).to eq '13:14:30'
    expect(response[:route][:locations][0][:adminArea5]).to eq 'Peoria'
    expect(response[:route][:locations][0][:adminArea3]).to eq 'IL'
    expect(response[:route][:locations][1][:adminArea5]).to eq 'Denver'
    expect(response[:route][:locations][1][:adminArea3]).to eq 'CO'
  end
end
