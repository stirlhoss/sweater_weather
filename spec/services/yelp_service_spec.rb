require 'rails_helper'

RSpec.describe YelpService do
  it 'returns a list of restuarants when given a location and a type of food', :vcr do
    response = YelpService.get_bussinesses("peoria,il", 'mexican')

    expect(response).to be_a Hash
    expect(response[:businesses]).to be_an Array
    expect(response[:businesses][0]).to be_an Hash
    expect(response[:businesses][0]).to have_key :name
    expect(response[:businesses][0]).to have_key :location
    expect(response[:businesses][0][:location]).to have_key :display_address
    expect(response[:businesses][0][:location][:display_address]).to eq [
                                                                          "4325 N Sheridan Rd",
                                                                          "Peoria, IL 61614"
                                                                        ]
  end
end
