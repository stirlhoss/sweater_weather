require 'rails_helper'

RSpec.describe OpenweatherService do
  it 'returns weather data for a given geocode', :vcr do
    response = OpenweatherService.get_weather(40.694592, -89.590362)

    expect(response).to be_a Hash
    expect(response[:current]).to be_a Hash
    expect(response[:current][:dt]).to be_an Integer
    expect(response[:current][:sunrise]).to be_an Integer
    expect(response[:current][:sunset]).to be_an Integer
    expect(response[:current][:temp]).to be_a Float
    expect(response[:current][:feels_like]).to be_a Float
    expect(response[:current][:humidity]).to be_an Integer
    expect(response[:current][:visibility]).to be_an Integer
    expect(response[:current]).to have_key :uvi
    expect(response[:current][:weather]).to be_an Array
    expect(response[:current][:weather][0]).to be_a Hash
    expect(response[:current][:weather][0][:description]).to be_a String
    expect(response[:current][:weather][0][:icon]).to be_a String

    expect(response[:daily]).to be_an Array
    expect(response[:daily][0][:dt]).to be_an Integer
    expect(response[:daily][0][:sunrise]).to be_an Integer
    expect(response[:daily][0][:sunset]).to be_an Integer
    expect(response[:daily][0][:temp]).to be_a Hash
    expect(response[:daily][0][:temp][:max]).to be_a Numeric
    expect(response[:daily][0][:temp][:min]).to be_a Numeric
    expect(response[:daily][0][:weather]).to be_an Array
    expect(response[:daily][0][:weather][0]).to be_a Hash
    expect(response[:daily][0][:weather][0][:description]).to be_a String
    expect(response[:daily][0][:weather][0][:icon]).to be_a String

    expect(response[:hourly]).to be_an Array
    expect(response[:hourly][0][:dt]).to be_an Integer
    expect(response[:hourly][0][:temp]).to be_an Float
    expect(response[:hourly][0][:weather][0]).to be_a Hash
    expect(response[:hourly][0][:weather][0][:description]).to be_a String
    expect(response[:hourly][0][:weather][0][:icon]).to be_a String
  end
end
