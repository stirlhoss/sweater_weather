require 'rails_helper'

RSpec.describe 'ForecastRequest' do
  describe 'happy path' do
    it 'sends serialized weather', :vcr do
      get '/api/v1/forecast?location=peoria,il'

      expect(response).to be_successful

      data = JSON.parse(response.body, symbolize_names: true)
      forecast = data[:data]

      expect(forecast[:id]).to eq nil
      expect(forecast[:type]).to eq 'forecast'

      current = forecast[:attributes][:current]
      daily = forecast[:attributes][:daily]
      hourly = forecast[:attributes][:hourly]

      expect(current.keys).to eq %i[datetime sunrise sunset temperature feels_like humidity uvi visibility conditions icon]
      expect(current).to_not have_key %i[clouds wind_speed]
      expect(daily[0].keys).to eq %i[date sunrise sunset max_temp min_temp conditions icon]
      expect(daily[0]).to_not have_key %i[moon_phase feels_like]
      expect(hourly[0].keys).to eq %i[time temperature conditions icon]
      expect(hourly[0]).to_not have_key %i[visibility wind_speed]
    end
  end

  describe 'sad path' do
    it 'sends an error when the location param is not passed', :vcr do
      get '/api/v1/forecast'

      expect(response.status).to eq 400
    end
  end
end
