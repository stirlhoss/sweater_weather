require 'rails_helper'

RSpec.describe 'MunchieRequest' do
  describe 'happy path' do
    it 'sends serialized munchie', :vcr do
      get '/api/v1/munchies?location=peoria,il&food=mexican'

      expect(response).to be_successful

      data = JSON.parse(response.body, symbolize_names: true)
      munchie = data[:data]

      expect(munchie[:id]).to eq nil
      expect(munchie[:type]).to eq 'munchie'
      expect(munchie[:attributes].keys).to eq %i[destination_city forecast restaurant]
    end
  end

  describe 'sad path' do
    it 'sends an error when food param is not passed', :vcr do
      get '/api/v1/munchies?location=peoria,il'

      expect(response.status).to eq 400
    end

    it 'sends an error when location param is not passed', :vcr do
      get '/api/v1/munchies?food=mexican'

      expect(response.status).to eq 400
    end

    it 'sends an error when no param is passed', :vcr do
      get '/api/v1/munchies'

      expect(response.status).to eq 400
    end
  end
end
