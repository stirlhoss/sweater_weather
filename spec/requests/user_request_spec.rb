require 'rails_helper'

RSpec.describe 'UserRequest' do
  describe 'happy path' do
    it 'creates a new user and receives an api token', :vcr do
      params = {
        "email": 'email@example.com',
        "password": 'password',
        "password_confirmation": 'password'
      }
      post '/api/v1/users', params: params

      expect(response).to be_successful

      results = JSON.parse(response.body, symbolize_names: true)
      user = User.last
      api_key = ApiKey.last

      expect(results[:data][:type]).to eq('user')
      expect(results[:data][:id]).to eq(user.id.to_s)
      expect(results[:data][:attributes]).to be_a(Hash)
      expect(results[:data][:attributes][:email]).to eq('email@example.com')
      expect(results[:data][:attributes]).to have_key(:api_key)
    end
  end

  describe 'sad path' do
    it 'returns an error when passwords dont match', :vcr do
      params = {
        "email": 'email@example.com',
        "password": 'password',
        "password_confirmation": 'word'
      }
      post '/api/v1/users', params: params

      expect(response.status).to eq 422

      results = JSON.parse(response.body, symbolize_names: true)

      expect(results[:password_confirmation][0]).to eq "doesn't match Password"
    end
  end
end
