require 'rails_helper'

RSpec.describe 'SessionRequest' do
  describe 'happy path' do
    it 'logs in a user and creates a session', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_keys.create(token: 'abc')

      params = {
        "email": 'email@example.com',
        "password": 'password',
      }

      post '/api/v1/sessions', params: params

      expect(response.status).to eq 200

      data = JSON.parse(response.body, symbolize_names: true)[:data]

      expect(data[:attributes].keys).to eq %i[email api_key]
    end
  end

  describe 'sad path' do
    it 'returns at error and a message when password is incorrect', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_keys.create(token: 'abc')

      params = {
        "email": 'email@example.com',
        "password": 'word',
      }

      post '/api/v1/sessions', params: params

      expect(response.status).to eq 400
      data = JSON.parse(response.body, symbolize_names: true)

      expect(data[:error]).to eq 'Invalid credentials'
    end

    it 'returns at error and a message when email is incorrect', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_keys.create(token: 'abc')

      params = {
        "email": 'example@example.com',
        "password": 'password',
      }

      post '/api/v1/sessions', params: params

      expect(response.status).to eq 400
      data = JSON.parse(response.body, symbolize_names: true)

      expect(data[:error]).to eq 'Invalid credentials'
    end
  end
end
