require 'rails_helper'

RSpec.describe 'RoadTripRequest' do
  describe 'happy path' do
    it 'sends serialized road trip', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_key

      params = {
        origin: 'Denver, CO',
        destination: 'Pueblo, CO',
        api_key: api_key
      }

      post '/api/v1/road_trip', params: params

      expect(response).to be_successful

      data = JSON.parse(response.body, symbolize_names: true)
      roadtrip = data[:data]

      expect(roadtrip[:id]).to eq nil
      expect(roadtrip[:type]).to eq 'road_trip'
      expect(roadtrip[:attributes][:start_city]).to eq 'Denver, CO'
      expect(roadtrip[:attributes][:end_city]).to eq 'Pueblo, CO'
      expect(roadtrip[:attributes][:travel_time]).to eq '1 hours, 45 minutes'
      expect(roadtrip[:attributes][:weather_at_eta][:temperature]).to eq 93.11
      expect(roadtrip[:attributes][:weather_at_eta][:conditions]).to eq 'scattered clouds'
    end
  end

  describe 'sad path' do
    it 'sends an error when the api key is invalid', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_key

      params = {
        origin: 'Denver, CO',
        destination: 'Pueblo, CO',
        api_key: 'brownies'
      }

      post '/api/v1/road_trip', params: params

      expect(response.status).to eq 401

      data = JSON.parse(response.body, symbolize_names: true)

      expect(data[:error]).to eq 'Invalid key'
    end

    it 'sends an error when the trip is impossible', :vcr do
      user1 = User.create(email: 'email@example.com', password: 'password')
      api_key = user1.api_key

      params = {
        origin: 'London, UK',
        destination: 'New York, NY',
        api_key: api_key
      }

      post '/api/v1/road_trip', params: params

      expect(response.status).to eq 400

      data = JSON.parse(response.body, symbolize_names: true)[:data]

      expect(data[:attributes][:travel_time]).to eq 'Impossible'
      expect(data[:attributes][:weather_at_eta][:temperature]).to eq ''
      expect(data[:attributes][:weather_at_eta][:conditions]).to eq ''
    end
  end
end
