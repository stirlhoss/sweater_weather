require 'rails_helper'

RSpec.describe Geocode do
  it 'creates a geocode poro', :vcr do
    geocode = MapquestFacade.get_geocode('peoria, il')

    expect(geocode).to be_a Geocode
    expect(geocode.lat).to be_a Float
    expect(geocode.lng).to be_a Float
  end
end
