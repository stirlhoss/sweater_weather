require 'rails_helper'

RSpec.describe RoadTrip do
  it 'returns a road trip based on the cities given', :vcr do
    trip = RoadTripFacade.get_road_trip('peoria,il', 'denver,co')

    expect(trip).to be_a(RoadTrip)

    expect(trip.start_city).to be_a String
    expect(trip.end_city).to be_a String
    expect(trip.travel_time).to be_a String
    expect(trip.weather_at_eta).to be_a Hash
    expect(trip.weather_at_eta[:temperature]).to be_a Float
    expect(trip.weather_at_eta[:conditions]).to be_a String
  end
end
