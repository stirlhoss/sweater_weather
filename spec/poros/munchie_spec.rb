require 'rails_helper'

RSpec.describe Munchie do
  it 'can be created and has the correct attributes', :vcr do
    location = "peoria, il"
    yelp = {
      businesses: [
      id: 'zJpFuB0b4rtnoB93CGWZpQ',
      alias: 'el-taco-loco-peoria',
      name: 'El Taco Loco',
      image_url: 'https://s3-media3.fl.yelpcdn.com/bphoto/8ouVIA-FkIlaW0r1LDwjKg/o.jpg',
      is_closed: false,
      url: 'https://www.yelp.com/biz/el-taco-loco-peoria?adjust_creative=zRhl8lDBcoSjhJjL3Vfv_A&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=zRhl8lDBcoSjhJjL3Vfv_A',
      review_count: 39,
      categories: [{ alias: 'mexican', title: 'Mexican' }],
      rating: 4.5,
      coordinates: { latitude: 40.74020383779, longitude: -89.6032379629629 },
      transactions: ['delivery'],
      location: {address1: '4325 N Sheridan Rd', address2: '', address3: nil, city: 'Peoria', zip_code: '61614',
                 country: 'US', state: 'IL', display_address: ['4325 N Sheridan Rd', 'Peoria, IL 61614'] },
      phone: '+13097131933',
      display_phone: '(309) 713-1933',
      distance: 1175.72743986766
      ]
    }
    weather = {
      lat: 40.6946,
      lon: -89.5904,
      timezone: 'America/Chicago',
      timezone_offset: -18000,
      current:
      {dt: 1659974672,
       sunrise: 1659956513,
       sunset: 1660007170,
       temp: 301.43,
       feels_like: 306.49,
       pressure: 1016,
       humidity: 83,
       dew_point: 298.26,
       uvi: 5.27,
       clouds: 75,
       visibility: 10000,
       wind_speed: 2.06,
       wind_deg: 290,
       weather: [{ id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }] }
    }

    munchie = Munchie.new(location, yelp, weather)

    expect(munchie.forecast[:summary]).to eq 'broken clouds'
    expect(munchie.forecast[:temperature]).to eq 301.43
    expect(munchie.forecast.keys).to_not eq %i[lat lon timezone]
    expect(munchie.restaurant[:name]).to eq 'El Taco Loco'
    expect(munchie.restaurant[:address]).to eq '4325 N Sheridan Rd, Peoria, IL 61614'
    expect(munchie.restaurant.keys).to_not eq %i[id alias url uvi clouds]
  end
end
