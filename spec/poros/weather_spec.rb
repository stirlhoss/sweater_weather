require 'rails_helper'

RSpec.describe Weather do
  describe 'testing specific values' do
    it 'creates a weather poro for current with the correct attributes', :vcr do
      weather = WeatherFacade.get_weather('peoria, il')

      expect(weather).to be_an_instance_of Weather
      expect(weather.current).to be_a Hash


      expect(weather.current[:datetime]).to be_a Time
      expect(weather.current[:sunrise]).to be_a Time
      expect(weather.current[:sunset]).to be_a Time
      expect(weather.current[:temperature]).to be_a Float
      expect(weather.current[:feels_like]).to be_a Float
      expect(weather.current[:humidity]).to be_a Integer
      expect(weather.current[:uvi]).to be_a Integer
      expect(weather.current[:visibility]).to be_a Integer
      expect(weather.current[:conditions]).to be_a String
      expect(weather.current[:icon]).to be_a String
      expect(weather.current.keys).to eq %i[datetime sunrise sunset temperature feels_like humidity uvi visibility conditions icon]
    end

    it 'creates a weather poro for daily with the correct attributes', :vcr do
      weather = WeatherFacade.get_weather('peoria, il')

      expect(weather).to be_an_instance_of Weather
      expect(weather.daily).to be_a Array
      expect(weather.daily[0]).to be_a Hash

      expect(weather.daily[0][:date]).to be_a String
      expect(weather.daily[0][:sunrise]).to be_a Time
      expect(weather.daily[0][:sunset]).to be_a Time
      expect(weather.daily[0][:max_temp]).to be_a Float
      expect(weather.daily[0][:min_temp]).to be_a Float
      expect(weather.daily[0][:conditions]).to be_a String
      expect(weather.daily[0][:icon]).to be_a String
      expect(weather.daily[0].keys).to eq %i[date sunrise sunset max_temp min_temp conditions icon]
    end

    it 'creates a weather poro for hourly with the correct attributes', :vcr do
      weather = WeatherFacade.get_weather('peoria, il')

      expect(weather).to be_an_instance_of Weather
      expect(weather.hourly).to be_a Array
      expect(weather.hourly[0]).to be_a Hash

      expect(weather.hourly[0][:time]).to be_a String
      expect(weather.hourly[0][:temperature]).to be_a Float
      expect(weather.hourly[0][:conditions]).to be_a String
      expect(weather.hourly[0][:icon]).to be_a String
    end
  end
end
