require 'rails_helper'

RSpec.describe MunchieFacade do
  it 'creates a yelp poro', :vcr do
    peoria = MunchieFacade.get_munchie('peoria, il', 'mexican')

    expect(peoria).to be_a Munchie
    expect(peoria.destination_city).to eq 'Peoria, Il'
    expect(peoria.forecast[:summary]).to eq 'overcast clouds'
    expect(peoria.forecast[:temperature]).to eq 75.65
    expect(peoria.restaurant[:address]).to eq '4325 N Sheridan Rd, Peoria, IL 61614'
    expect(peoria.restaurant[:name]).to eq 'El Taco Loco'
  end
end
