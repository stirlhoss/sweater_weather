require 'rails_helper'

RSpec.describe MapquestFacade, type: :facade do
  it 'creates a geocode poro', :vcr do
    geocode = MapquestFacade.get_geocode('peoria, il')

    expect(geocode).to be_a Geocode
  end
end
