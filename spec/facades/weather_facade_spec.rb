require 'rails_helper'

RSpec.describe WeatherFacade, type: :facade do
  describe 'get_forecast' do
    it 'gets the weather for a location and creates a weather poro', :vcr do
      weather = WeatherFacade.get_weather('peoria, il')

      expect(weather).to be_an_instance_of Weather
    end
  end
end
