require 'rails_helper'

RSpec.describe RoadTripFacade, type: :facade do
  describe 'get_road_trip' do
    it 'returns a road trip based on the cities given', :vcr do
      trip = RoadTripFacade.get_road_trip('peoria,il', 'denver,co')

      expect(trip).to be_a(RoadTrip)

      expect(trip.start_city).to eq 'Peoria, IL'
      expect(trip.end_city).to eq 'Denver, CO'
      expect(trip.travel_time).to eq '13 hours, 14 minutes'
      expect(trip.weather_at_eta).to be_a Hash
      expect(trip.weather_at_eta[:temperature]).to eq 73.96
      expect(trip.weather_at_eta[:conditions]).to eq 'clear sky'
    end

    it 'returns a road trip based on the cities given even if it ir more than 2 days', :vcr do
      trip = RoadTripFacade.get_road_trip('New york city,ny', 'panama city,pa')

      expect(trip).to be_a(RoadTrip)

      expect(trip.start_city).to eq 'New York, NY'
      expect(trip.end_city).to eq 'Panama, PA'
      expect(trip.travel_time).to eq '80 hours, 4 minutes'
      expect(trip.weather_at_eta).to be_a Hash
      expect(trip.weather_at_eta[:temperature]).to eq 82.53
      expect(trip.weather_at_eta[:conditions]).to eq 'moderate rain'
    end
  end
end
